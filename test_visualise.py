from pyosim.OsimModel import OsimModel

## uncomment for different models
# one arm
# model_path = './opensim_models/upper_body/unimanual/MoBL-ARMS Upper Extremity Model/MOBL_ARMS_fixed_41.osim'
# model_path = './opensim_models/upper_body/unimanual/arm26.osim'
# model_path = './opensim_models/upper_body/unimanual/OSarm412.osim'
# model_path = './opensim_models/upper_body/unimanual/Wu_Shoulder_Model.osim'

# both arms
# model_path = './opensim_models/upper_body/bimanual/MoBL_ARMS_bimanual_6_2_21.osim'
# model_path = './opensim_models/upper_body/bimanual/full_upper_body_marks.osim'

# full body
model_path = './opensim_models/full_body/gait2392_simbody.osim'

# lower bodyy
# model_path = './opensim_models/lower_body/leg6dof9musc.osim'

## Constructor of the OsimModel class.
OsimModel(model_path,visualize=True).displayModel()



